package com.hc.plugintower.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.hc.plugintower.base.BaseViewModel
import com.hc.plugintower.data.model.MonitorListDataModel
import com.hc.plugintower.repository.MonitorRepository
import kotlinx.coroutines.flow.collectLatest


/**
 * SimpleDes:
 * Creator: Sindi
 * Date: 2021-06-30
 * UseDes:
 */
class MonitorVM : BaseViewModel() {

    private val mMonitorRepository: MonitorRepository by lazy { MonitorRepository(mApiService) }

    private val _mMonitorListDataModel = MutableLiveData<PagingData<MonitorListDataModel>>()
    val mMonitorList: LiveData<PagingData<MonitorListDataModel>> = _mMonitorListDataModel

    /*设备列表*/
    fun getMonitorList(proUuid: String) = ycLaunch {
        mMonitorRepository.getMonitorList("500246").cachedIn(viewModelScope).collectLatest {//TODO 修改"500246"
            _mMonitorListDataModel.postValue(it)
        }
    }

}