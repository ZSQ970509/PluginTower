package com.hc.plugintower.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.recyclerview.widget.DiffUtil
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewbinding.ViewBinding
import com.yc.jetpacklib.data.constant.YcNetErrorCode
import com.yc.jetpacklib.exception.YcException


/**
 *
 */
abstract class BaseRefreshAdapter<Data : Any, VB : ViewBinding>(
    createVB: (LayoutInflater, ViewGroup?, Boolean) -> VB,
    diffCallback: DiffUtil.ItemCallback<Data>,
    private val mSwipeRefreshLayout: SwipeRefreshLayout,
) : BaseAdapter<Data, VB>(createVB, diffCallback) {
    init {
        addLoadStateListener {
            // 这里的逻辑可以自由发挥
            when (it.refresh) {
                is LoadState.Error -> {
                    mSwipeRefreshLayout.isRefreshing = false
                    val errState = it.refresh as? LoadState.Error ?: it.append as? LoadState.Error
                    val errorCode = (errState?.error as YcException).code
                    if (errorCode == YcNetErrorCode.DATE_NULL) {
                        //TODO
                    }
                }
                is LoadState.Loading -> {

                }
                is LoadState.NotLoading -> {
                    mSwipeRefreshLayout.isRefreshing = false
                }
            }
        }

    }
}