package com.hcc.plugindust.ui.monitor


import android.content.Intent
import androidx.activity.result.ActivityResultLauncher
import com.example.plugintower.databinding.CommonMonitorListActivityBinding
import com.hc.plugintower.adapter.ElevatorMonitorPageAdapter
import com.hc.plugintower.base.BaseActivity
import com.hc.plugintower.viewmodel.MonitorVM
import com.yc.jetpacklib.extension.ycCreateResultLauncher
import com.yc.jetpacklib.refresh.YcRefreshBaseUtil
import com.yc.jetpacklib.refresh.YcRefreshSpecialViewUtil
import com.yc.jetpacklib.release.YcSpecialViewSmart

/**
 * SimpleDes:
 * Creator: Sindi
 * Date: 2021-07-13
 * UseDes:升降机监测
 */
class ElevatorMonitorActivity : BaseActivity<CommonMonitorListActivityBinding>(CommonMonitorListActivityBinding::inflate) {

    private val mViewModel: MonitorVM by ycViewModels()
    private var mProUuid: String? = null
    private lateinit var mSpecialView: YcSpecialViewSmart
    private lateinit var mRefreshUtil: YcRefreshBaseUtil

    private val mAdapter by lazy {
        ElevatorMonitorPageAdapter()
    }


    override fun CommonMonitorListActivityBinding.initView(){
        actionbarLayout.setTitle(titleName = "升降机监测")
       /* App.mInstant.viewModel.mProInfoEntity.observe {
            if (it == null || it.proUuid.ycIsEmpty()) {
                mProUuid = null
            } else {
                mProUuid = it.proUuid!!
                getData()
            }
        }*/


        mSpecialView = YcSpecialViewSmart(rvMonitorList, flRefresh)
        mRefreshUtil = YcRefreshSpecialViewUtil(this@ElevatorMonitorActivity).build(mAdapter, srlMonitor, rvMonitorList, mSpecialView) {
            mRefreshCall = { getData() }
        }

        mViewModel.mMonitorList.observe {
            mAdapter.submitData(this@ElevatorMonitorActivity.lifecycle, it)
        }

    }

    private fun CommonMonitorListActivityBinding.getData() {
       /* mViewModel.getMonitorList(mProUuid!!)*/

    }

}