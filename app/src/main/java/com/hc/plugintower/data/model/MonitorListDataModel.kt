package com.hc.plugintower.data.model

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import kotlinx.android.parcel.Parcelize

/**
 * belongAreaCode : 天津市市辖区南开区
 * dataSource : 2
 * deviceCount : 3
 * generalCompanyId : 165
 * generalCompanyName : 省站测试企业
 * parentProjectId : 0
 * proStatus : 002
 * projectId : 133
 * projectName : 省站测试
 * teamCount : 9
 * thirdProjectId : 223125
 * workerCount : 19
 */


@Parcelize
data class MonitorListDataModel(
    var deviceId: String?,
    var relDeviceId: String?,
    var deviceName: String,
    var deviceType: String,
    var deviceSn: String,
    var installPlace: String,
    var onlineStatus: Int,
    var relDeviceName: String,
    var physicalStatus: String,
    var dataSource: String,
) : Parcelable {
    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<MonitorListDataModel>() {
            override fun areItemsTheSame(oldItem: MonitorListDataModel, newItem: MonitorListDataModel): Boolean {
                return oldItem.deviceId == newItem.deviceId
            }

            override fun areContentsTheSame(
                oldItem: MonitorListDataModel,
                newItem: MonitorListDataModel
            ): Boolean {
                return oldItem.deviceId == newItem.deviceId
                        && oldItem.relDeviceId == newItem.relDeviceId
                        && oldItem.deviceName == newItem.deviceName
                        && oldItem.deviceType == newItem.deviceType
                        && oldItem.deviceSn == newItem.deviceSn
                        && oldItem.installPlace == newItem.installPlace
                        && oldItem.onlineStatus == newItem.onlineStatus
                        && oldItem.relDeviceName == newItem.relDeviceName
                        && oldItem.physicalStatus == newItem.physicalStatus
                        && oldItem.dataSource == newItem.dataSource
            }
        }
    }
}

