package com.hc.plugintower.base

import com.hc.plugintower.net.ApiService
import com.yc.jetpacklib.base.YcBaseViewModel
import com.yc.jetpacklib.net.YcRetrofitUtil

/**
 * Creator: yc
 * Date: 2021/6/17 18:26
 * UseDes:
 */
open class BaseViewModel : YcBaseViewModel() {
    protected val mApiService: ApiService by lazy {
        YcRetrofitUtil.Instance.getApiService(ApiService::class.java)
    }
}