package com.hc.plugintower.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.viewbinding.ViewBinding
import com.example.plugintower.R
import com.example.plugintower.databinding.ActionbarLayoutBinding
import com.gyf.immersionbar.BarHide
import com.gyf.immersionbar.ImmersionBar
import com.yc.jetpacklib.base.YcBaseActivityPlus


/**
 * Creator: yc
 * Date: 2021/6/17 18:26
 * UseDes:
 */
abstract class BaseActivity<VB : ViewBinding>(createVB: ((LayoutInflater) -> VB)? = null) : YcBaseActivityPlus<VB>(createVB) {

    protected fun viewBind(block: VB.(VB) -> Unit) {
        mViewBinding.apply { block(this) }
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initImmersionBar()
    }

    open fun initImmersionBar() {
        ImmersionBar.with(this)
            .statusBarColor(R.color.white)
            .statusBarDarkFont(true)
            .hideBar(BarHide.FLAG_SHOW_BAR)
            .fullScreen(false)
            .fitsSystemWindows(true)
            .init()
    }


    /**
     * 设置标题
     * @param titleName String                  标题名
     * @param leftClick Function1<View, Unit>?  左侧点击事件，空则默认finish
     * @param rightClick Function1<View, Unit>? 右侧点击事件
     * @param rightTv String?                   右侧文字
     * @param rightIv Int?                      右侧图片
     */
    protected fun ActionbarLayoutBinding.setTitle(
        titleName: String = "",
        leftClick: ((View) -> Unit)? = null,
        rightClick: ((View) -> Unit)? = null,
        rightTv: String? = null,
        rightIv: Int? = null
    ) {
        tvActionbarMid.text = titleName
        ivActionbarLeft.setOnClickListener {
            if (leftClick == null) {
                finish()
            } else {
                leftClick(it)
            }
        }
        rightTv?.let { name ->
            tvActionbarRight.apply {
                visibility = View.VISIBLE
                text = name
                setOnClickListener { v ->
                    if (rightClick != null) {
                        rightClick(v)
                    }
                }
            }
        }
        rightIv?.let { resId ->
            ivActionbarRight.apply {
                visibility = View.VISIBLE
                setImageResource(resId)
                setOnClickListener { v ->
                    if (rightClick != null) {
                        rightClick(v)
                    }
                }
            }
        }
    }

}