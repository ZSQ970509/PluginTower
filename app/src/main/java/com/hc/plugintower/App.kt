package com.hc.plugintower

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import com.hc.plugintower.net.CookieIntercept
import com.hc.plugintower.viewmodel.AppViewModel
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.yc.jetpacklib.init.YcJetpack
import com.yc.jetpacklib.refresh.YcRefreshHeaderView


/**
 * Creator: yc
3
 * UseDes: * Date: 2021/6/24 17:1
 */
class App : Application() {

    companion object {
        lateinit var mInstant: App
            private set
    }

   val viewModel by lazy {
        ViewModelProvider.AndroidViewModelFactory(this).create(AppViewModel::class.java)
    }
   /*  val mRefreshVM by lazy {
        ViewModelProvider.AndroidViewModelFactory(this).create(RefreshVM::class.java)
    }*/

    override fun onCreate() {
        super.onCreate()
        mInstant = this
        YcJetpack.mInstance.apply {
            init(this@App)
            SmartRefreshLayout.setDefaultRefreshHeaderCreator { context, _ -> YcRefreshHeaderView(context) }
            setBaseUrl("http://10.55.21.74:8300/")//TODO url
            addInterceptor(CookieIntercept())
        }
    }
}