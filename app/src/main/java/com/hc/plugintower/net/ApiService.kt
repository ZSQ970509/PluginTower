package com.hc.plugintower.net

import com.hc.plugintower.data.json.MonitorJson
import com.hcc.plugindust.net.HttpResponseList
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    /**
     * 监测列表
     */
    @GET("device/list")
    suspend fun getMonitorList(
        @Query("projectUuid") proUuid: String,
        @Query("page") pageIndex: Int,
        @Query("limit") pageSize: Int
    ): HttpResponseList<MonitorJson>

}