package com.hc.pluginlabour.net

/**
 * Creator: yc
 * Date: 2021/2/8 14:14
 * UseDes: 网络请求特殊的code
 */
object NetCode {
    /**
     * hcc接口token无效，返回的code
     */
    const val NET_CODE_TOKEN_INVALID = 401
    /**
     * 网络请求成功码
     */
    const val NET_CODE_SUCCESS = 200


}