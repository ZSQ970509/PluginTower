package com.hc.plugintower.data.json

import com.google.gson.annotations.SerializedName


/**
 * createTime : 2020-09-15T07:04:56.752Z
 * createUser : 0
 * deviceFactory : string
 * deviceId : 0
 * deviceName : string
 * deviceSn : string
 * deviceType : string
 * deviceTypeName : string
 * deviceUuid : string
 * installPlace : string
 * onlineStatus : 0
 * projectName : string
 * projectUuid : string
 * updateTime : 2020-09-15T07:04:56.752Z
 * updateUser : 0
 * relDeviceName
 */
data class MonitorJson(
    var createTime: String?,
    var createUser: String?,
    var deviceFactory: String?,
    var deviceId: String?,
    var deviceName: String?,
    var deviceSn: String?,
    var deviceType: String?,
    var deviceTypeName: String?,
    var deviceUuid: String?,
    var installPlace: String?,
    var onlineStatus: Int?,
    var projectName: String?,
    var projectUuid: String?,
    var updateTime: String?,
    var updateUser: String?,
    var relDeviceName: String?,
    @SerializedName("mdeviceUuid")  var relDeviceId: String?,
    var dataSource: String?,
    var physicalStatus: String?,
)