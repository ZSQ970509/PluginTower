package com.hc.plugintower.adapter

import com.example.plugintower.databinding.CommonMonitorListItemBinding
import com.hc.plugintower.data.model.MonitorListDataModel
import com.yc.jetpacklib.recycleView.YcPagingDataAdapterPlus


/*监测列表*/

class ElevatorMonitorPageAdapter() : YcPagingDataAdapterPlus<MonitorListDataModel, CommonMonitorListItemBinding>(
    CommonMonitorListItemBinding::inflate,
    MonitorListDataModel.diffCallback
) {
    override fun CommonMonitorListItemBinding.onUpdate(position: Int, data: MonitorListDataModel) {

    }
}
