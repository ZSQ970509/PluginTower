package com.hc.plugintower.base

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.viewbinding.ViewBinding
import com.yc.jetpacklib.recycleView.YcPagingDataAdapter
import com.yc.jetpacklib.recycleView.YcViewHolder

/**
 *
 */
abstract class BaseAdapter<Data : Any, VB : ViewBinding>(
    createVB: (LayoutInflater, ViewGroup?, Boolean) -> VB,
    diffCallback: DiffUtil.ItemCallback<Data>
) : YcPagingDataAdapter<Data, VB>(createVB, diffCallback) {
    protected lateinit var mViewHolder: YcViewHolder<VB>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): YcViewHolder<VB> {
        mViewHolder = super.onCreateViewHolder(parent, viewType)
        return mViewHolder
    }

    fun getContext(): Context = mViewHolder.itemView.context

}