package com.hc.plugintower.data.mapper


import com.hc.plugintower.data.json.MonitorJson
import com.hc.plugintower.data.model.MonitorListDataModel
import com.yc.jetpacklib.extension.ycToNoEmpty
import com.yc.jetpacklib.mapper.IMapper

class MonitorListJsonToModel : IMapper<MonitorJson, MonitorListDataModel> {
    override fun map(input: MonitorJson): MonitorListDataModel = MonitorListDataModel(
        input.deviceId,
        input.relDeviceId,
        input.deviceName.ycToNoEmpty(),
        input.deviceType.ycToNoEmpty(),
        input.deviceSn.ycToNoEmpty(),
        input.installPlace.ycToNoEmpty(),
        input.onlineStatus.ycToNoEmpty(0),
        input.relDeviceName.ycToNoEmpty(),
        input.physicalStatus.ycToNoEmpty(),
        input.dataSource.ycToNoEmpty(),
    )
}