package com.hc.plugintower.repository

import checkListDataTotal
import com.hc.plugintower.data.mapper.MonitorListJsonToModel
import com.hc.plugintower.net.ApiService
import com.yc.jetpacklib.base.YcRepository
import com.yc.jetpacklib.recycleView.YcPager


/**
 * SimpleDes:
 * Creator: Sindi
 * Date: 2021-06-30
 * UseDes:
 */
class MonitorRepository(private val apiService: ApiService) : YcRepository() {


    /**
     * 获取设备列表
     */
    private val mMonitorListJsonToModel by lazy { MonitorListJsonToModel() }
    fun getMonitorList(proUuid: String) = YcPager.getPagerFlow { pageIndex, pageSize ->
        apiService.getMonitorList(proUuid, pageIndex, pageSize).checkListDataTotal(mMonitorListJsonToModel)
    }

}