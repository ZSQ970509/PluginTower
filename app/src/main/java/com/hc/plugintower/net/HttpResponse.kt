package com.hcc.plugindust.net

import com.google.gson.annotations.SerializedName

/**
 * 请求的返回数据（通用的）
 */

data class HttpResponse<T>(
    @SerializedName("message") var message: String?,//描述信息
    @SerializedName("code") var code: Int,//状态码
    @SerializedName("data") var data: T?,//数据对象[成功返回对象,失败返回错误说明]
)


/**
 * 请求的返回数据(有分页的)
 * @property totalNum Int 数据总数（不是总页数）
 * @property data List<T>?
 */
data class HttpResponseList<T>(
    @SerializedName("code") val code: Int,
    @SerializedName("message") val message: String?,
    @SerializedName("count") val totalNum: Int,
    @SerializedName("data") val data: List<T>?
)
